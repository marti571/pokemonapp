using System;
using System.Collections.Generic;
using Plugin.Connectivity;
using System.Net.Http;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace PokemonApp
{
    public partial class PokedexPage : ContentPage
    {

        // hi
        PokemonData[] NormalArray = new PokemonData[26];
        PokemonData[] BugArray = new PokemonData[14];
        PokemonData[] DragonArray = new PokemonData[5];
        PokemonData[] IceArray = new PokemonData[7];
        PokemonData[] FightingArray = new PokemonData[10];
        PokemonData[] FireArray = new PokemonData[14];
        PokemonData[] FlyingArray = new PokemonData[21];
        PokemonData[] GrassArray = new PokemonData[16];
        PokemonData[] GhostArray = new PokemonData[5];
        PokemonData[] GroundArray = new PokemonData[16];
        PokemonData[] ElectricArray = new PokemonData[11];
        PokemonData[] PoisonArray = new PokemonData[35];
        PokemonData[] PsychicArray = new PokemonData[16];
        PokemonData[] RockArray = new PokemonData[13];
        PokemonData[] WaterArray = new PokemonData[34];
        PokemonData[] FairyArray = new PokemonData[6];


        public int f = 0;//first type
        public int s = 1;//second type
        public int nml = 0;//for the normal array
        public int bug = 0;//for bug array
        public int dra = 0;//for dragon array
        public int ice = 0;//for ice array
        public int fght = 0;//for fighting array
        public int fire = 0;//for fire array
        public int flying = 0;//for the flying array
        public int grass = 0;//for the grass array
        public int ghost = 0;//for ghost array
        public int ground = 0;//for ground array
        public int electric = 0;//for electric array
        public int poison = 0;//for poison array
        public int psychic = 0;//for the psychic array
        public int rock = 0;//for the rock array
        public int water = 0;//for the water array
        public int fairy = 0;

        //fire type
        public int f1 = 4;
        public int f2 = 5;
        public int f3 = 37;
        public int f4 = 38;
        public int f5 = 58;
        public int f6 = 77;
        public int f7 = 78;
        public int f8 = 126;
        public int f9 = 136;
        public int f10 = 59;

        //normal types
        public int n1 = 19;
        public int n2 = 20;

        public int n5 = 39;
        public int n6 = 40;
        public int n7 = 52;
        public int n8 = 53;
        public int n9 = 108;
        public int n10 = 113;
        public int n11 = 115;
        public int n12 = 128;
        public int n13 = 132;
        public int n14 = 133;
        public int n15 = 137;
        public int n16 = 143;

        //bug types
        public int b1 = 10;
        public int b2 = 11;
        public int b3 = 127;

        //dragon types
        public int d1 = 147;
        public int d2 = 148;

        //fighting types
        public int fi1 = 56;
        public int fi2 = 57;
        public int fi3 = 66;
        public int fi4 = 67;
        public int fi5 = 68;
        public int fi6 = 106;
        public int fi7 = 107;

        //grass types
        public int gr = 114;

        //ground types
        public int gro1 = 27;
        public int gro2 = 28;
        public int gro3 = 50;
        public int gro4 = 51;
        public int gro5 = 104;
        public int gro6 = 105;

        //electric
        public int e1 = 25;
        public int e2 = 26;
        public int e3 = 100;
        public int e4 = 101;
        public int e5 = 125;
        public int e6 = 135;

        //water
        public int w1 = 7;
        public int w2 = 8;
        public int w3 = 9;
        public int w4 = 54;
        public int w5 = 55;
        public int w6 = 60;
        public int w7 = 61;
        public int w8 = 86;
        public int w9 = 90;
        public int w10 = 98;
        public int w11 = 99;
        public int w12 = 116;
        public int w13 = 117;
        public int w14 = 118;
        public int w15 = 119;
        public int w16 = 120;
        public int w17 = 129;
        public int w18 = 134;

        //psychic
        public int p1 = 63;
        public int p2 = 64;
        public int p3 = 65;
        public int p4 = 96;
        public int p5 = 97;
        public int p6 = 122;
        public int p7 = 150;
        public int p8 = 151;

        //poison
        public int psn1 = 23;
        public int psn2 = 24;
        public int psn3 = 29;
        public int psn4 = 30;
        public int psn5 = 32;
        public int psn6 = 33;
        public int psn7 = 88;
        public int psn8 = 89;
        public int psn9 = 109;
        public int psn10 = 110;

        //fairy
        public int fa1 = 35;
        public int fa2 = 36;



        public PokedexPage()
        {
            InitializeComponent();
            GetPokemon();
            //http://pokeapi.co/api/v2/pokemon/1/ note that this calls "bulbasaur".
        }

        async private void GetPokemon()
        {
            //Main array that stores the 151 first pokemon, 1st generation
            PokemonData[] pokeArray = new PokemonData[152];

            HttpClient client1 = new HttpClient();
            HttpClient client2 = new HttpClient();
            HttpClient client3 = new HttpClient();
            HttpClient client4 = new HttpClient();


            HttpRequestMessage requestt1 = new HttpRequestMessage();
            HttpRequestMessage requestt2 = new HttpRequestMessage();
            HttpRequestMessage requestt3 = new HttpRequestMessage();
            HttpRequestMessage requestt4 = new HttpRequestMessage();


            /*
            int setter1 = 1;
            int setter2 = 151;
            */
            for (int i = 1; i <= 50; i++)
            {
                if (CrossConnectivity.Current.IsConnected == true)
                {
                    finished.Text = "Loading: " + i + "/151";
                    var uri = new Uri(string.Format($"http://pokeapi.co/api/v2/pokemon/{i}/"));

                    requestt1 = new HttpRequestMessage();

                    requestt1.Method = HttpMethod.Get;
                    requestt1.RequestUri = uri;
                    requestt1.Headers.Add("Application", "application / json");

                    HttpResponseMessage response = await client1.SendAsync(requestt1);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        pokeArray[i] = PokemonData.FromJson(content);

                    }
                }
                else
                {
                    bool answer = await DisplayAlert("Alert", "No Internet Connection", "OK", "Cancel");
                    if(answer == true)
                    {
                        await Navigation.PushAsync(new PokedexPage());
                    }
                }
            }

            for (int i = 51; i <= 100; i++)
            {

                if (CrossConnectivity.Current.IsConnected == true)
                {
                    finished.Text = "Loading: " + i + "/151";
                    var uri = new Uri(string.Format($"http://pokeapi.co/api/v2/pokemon/{i}/"));

                    requestt2 = new HttpRequestMessage();

                    requestt2.Method = HttpMethod.Get;
                    requestt2.RequestUri = uri;
                    requestt2.Headers.Add("Application", "application / json");

                    HttpResponseMessage response = await client2.SendAsync(requestt2);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        pokeArray[i] = PokemonData.FromJson(content);

                    }
                }
                else
                {
                    bool answer = await DisplayAlert("Alert", "No Internet Connection", "OK", "Cancel");
                    if (answer == true)
                    {
                        await Navigation.PushAsync(new PokedexPage());
                    }
                }
            }
            for (int i = 101; i <= 125; i++)
            {
                if (CrossConnectivity.Current.IsConnected == true)
                {
                    finished.Text = "Loading: " + i + "/151";
                    var uri = new Uri(string.Format($"http://pokeapi.co/api/v2/pokemon/{i}/"));

                    requestt3 = new HttpRequestMessage();

                    requestt3.Method = HttpMethod.Get;
                    requestt3.RequestUri = uri;
                    requestt3.Headers.Add("Application", "application / json");

                    HttpResponseMessage response = await client3.SendAsync(requestt3);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        pokeArray[i] = PokemonData.FromJson(content);

                    }
                }
                else
                {
                    bool answer = await DisplayAlert("Alert", "No Internet Connection", "OK", "Cancel");
                    if (answer == true)
                    {
                        await Navigation.PushAsync(new PokedexPage());
                    }
                }
            }
            for (int i = 126; i <= 151; i++)
            {
                if (CrossConnectivity.Current.IsConnected == true)
                {
                    finished.Text = "Loading: " + i + "/151";
                    var uri = new Uri(string.Format($"http://pokeapi.co/api/v2/pokemon/{i}/"));

                    requestt4 = new HttpRequestMessage();

                    requestt4.Method = HttpMethod.Get;
                    requestt4.RequestUri = uri;
                    requestt4.Headers.Add("Application", "application / json");

                    HttpResponseMessage response = await client4.SendAsync(requestt4);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        pokeArray[i] = PokemonData.FromJson(content);

                    }
                }
                else
                {
                    bool answer = await DisplayAlert("Alert", "No Internet Connection", "OK", "Cancel");
                    if (answer == true)
                    {
                        await Navigation.PushAsync(new PokedexPage());
                    }
                }
            }

            

            for (int j = 1; j <= 151; j++)
            {
                if(j == fa1)
                {
                    FairyArray[fairy] = pokeArray[j];
                    fairy++;
                }
                else if(j == fa2)
                {
                    FairyArray[fairy] = pokeArray[j];
                    fairy++;
                }

                else if (pokeArray[j].Types[f].Type.Name == "fairy")
                {
                    FairyArray[fairy] = pokeArray[j];
                    fairy++;
                    if (pokeArray[j].Types[s].Type.Name != null)
                    { sortPokemon(pokeArray[j]); }
                }

                

                else if (pokeArray[j].Types[f].Type.Name == "normal")
                {
                    NormalArray[nml] = pokeArray[j];
                    nml++;
                    if(j == n1)
                    {}
                    else if (j == n2)
                    { }
                    else if (j == n5)
                    { }
                    else if (j == n6)
                    { }
                    else if (j == n7)
                    { }
                    else if (j == n8)
                    { }
                    else if (j == n9)
                    { }
                    else if (j == n10)
                    { }
                    else if (j == n11)
                    { }
                    else if (j == n12)
                    { }
                    else if (j == n13)
                    { }
                    else if (j == n14)
                    { }
                    else if (j == n15)
                    { }
                    else if (j == n16)
                    { }
                    else
                    { sortPokemon(pokeArray[j]); }


                   
                     

                }

                else if (pokeArray[j].Types[f].Type.Name == "bug")
                {
                    BugArray[bug] = pokeArray[j];
                    bug++;
                    if (j == b1)
                    {}
                    else if (j == b2)
                    {}
                    else if(j == b3)
                    {}
                    else
                    {
                      sortPokemon(pokeArray[j]);
                    }  


                      
                }

                else if (pokeArray[j].Types[f].Type.Name == "dragon")
                {
                    DragonArray[dra] = pokeArray[j];
                    dra++;
                    if (j == d1)
                    {}
                    else if (j == d2)
                    {}
                    else{
                        sortPokemon(pokeArray[j]);
                    }
                }

                else if (pokeArray[j].Types[f].Type.Name == "ice")
                {
                    IceArray[ice] = pokeArray[j];
                    ice++;
                    if (pokeArray[j].Types[s].Type.Name != null)
                     sortPokemon(pokeArray[j]); 
                }

                else if (pokeArray[j].Types[f].Type.Name == "fighting")
                {
                    FightingArray[fght] = pokeArray[j];
                    fght++;
                    if (j == fi1)
                    {}
                    else if (j == fi2)
                    { }
                    else if (j == fi3)
                    { }
                    else if (j == fi4)
                    { }
                    else if (j == fi5)
                    { }
                    else if (j == fi6)
                    { }
                    else if (j == fi7)
                    { }
                    else{
                        sortPokemon(pokeArray[j]); 
                    }

                      
                }

                else if (pokeArray[j].Types[f].Type.Name == "fire")
                {
                    


                    FireArray[fire] = pokeArray[j];
                    fire++;
                    if (j == f1)
                    {
                        
                    }
                    else if (j == f2)
                    {
                        
                    }
                    else if (j == f3)
                    {

                    }
                    else if (j == f4)
                    {

                    }
                    else if (j == f5)
                    {

                    }
                    else if (j == f6)
                    {

                    }
                    else if (j == f7)
                    {

                    }
                    else if (j == f8)
                    {

                    }
                    else if (j == f9)
                    {

                    }
                    else if (j == f10)
                    {
                        
                    }
                    else
                    { sortPokemon(pokeArray[j]);}
                
                }


                else if (pokeArray[j].Types[f].Type.Name == "flying")
                {
                    FlyingArray[flying] = pokeArray[j];
                    flying++;
                    if (pokeArray[j].Types[s].Type.Name != null)
                    { sortPokemon(pokeArray[j]); }
                }

                else if (pokeArray[j].Types[f].Type.Name == "grass")
                {
                    GrassArray[grass] = pokeArray[j];
                    grass++;
                    if (j == gr)
                    {}
                    else{
                        sortPokemon(pokeArray[j]);
                    }

                      
                }

                else if (pokeArray[j].Types[f].Type.Name == "ghost")
                {
                    GhostArray[ghost] = pokeArray[j];
                    ghost++;
                    if (pokeArray[j].Types[s].Type.Name != null)
                    { sortPokemon(pokeArray[j]); }
                }

                else if (pokeArray[j].Types[f].Type.Name == "ground")
                {
                    GroundArray[ground] = pokeArray[j];
                    ground++;
                    if (j == gro1)
                    {}
                    else if (j == gro2)
                    { }
                    else if (j == gro3)
                    { }
                    else if (j == gro4)
                    { }
                    else if (j == gro5)
                    { }
                    else if (j == gro6)
                    { }
                    else
                    {
                        sortPokemon(pokeArray[j]);

                    }

                      
                }

                else if (pokeArray[j].Types[f].Type.Name == "electric")
                {
                    ElectricArray[electric] = pokeArray[j];
                    electric++;
                    if (j == e1)
                    {}
                    else if (j == e2)
                    { }
                    else if (j == e3)
                    { }
                    else if (j == e4)
                    { }
                    else if (j == e5)
                    { }
                    else if (j == e6)
                    { }
                    else
                    {
                        sortPokemon(pokeArray[j]); 
                    }


                      
                }

                else if (pokeArray[j].Types[f].Type.Name == "poison")
                {
                    PoisonArray[poison] = pokeArray[j];
                    poison++;
                    if (j == psn1)
                    { }
                    else if (j == psn2)
                    { }
                    else if (j == psn3)
                    { }
                    else if (j == psn4)
                    { }
                    else if (j == psn5)
                    { }
                    else if (j == psn6)
                    { }
                    else if (j == psn7)
                    { }
                    else if (j == psn8)
                    { }
                    else if (j == psn9)
                    { }
                    else if (j == psn10)
                    { }
                    else
                    {
                        sortPokemon(pokeArray[j]);
                    }

                }

                else if (pokeArray[j].Types[f].Type.Name == "psychic")
                {
                    PsychicArray[psychic] = pokeArray[j];
                    psychic++;
                    if (j == p1)
                    { }
                    else if (j == p2)
                    { }
                    else if (j == p3)
                    { }
                    else if (j == p4)
                    { }
                    else if (j == p5)
                    { }
                    else if (j == p6)
                    { }
                    else if (j == p7)
                    { }
                    else if (j == p8)
                    { }
                    else
                    {
                        sortPokemon(pokeArray[j]);
                    }

                }

                else if (pokeArray[j].Types[f].Type.Name == "rock")
                {
                    RockArray[rock] = pokeArray[j];
                    rock++;
                    if (pokeArray[j].Types[s].Type.Name != null)
                    { sortPokemon(pokeArray[j]); }
                }

                else if(pokeArray[j].Types[f].Type.Name == "water")
                {
                    WaterArray[water] = pokeArray[j];
                    water++;
                    if(j==w1)
                    {
                        
                    }
                    else if(j==w2)
                    {}
                    else if(j==w3)
                    {}
                    else if(j==w4)
                    {}
                    else if(j==w5)
                    {}
                    else if(j==w6)
                    {}
                    else if(j==w7)
                    {}
                    else if(j==w8)
                    {}
                    else if(j==w9)
                    {}
                    else if(j==w10)
                    {}
                    else if(j==w11)
                    {}
                    else if(j==w12)
                    {}
                    else if(j==w13)
                    {}
                    else if(j==w14)
                    {}
                    else if(j==w15)
                    {}
                    else if(j==w16)
                    {}
                    else if(j==w17)
                    {}
                    else if(j==w18)
                    {}
                    else{
                    sortPokemon(pokeArray[j]);}

                    

                }
                else
                {
                    sortPokemon(pokeArray[j]);
                }


            }
            finished.TextColor = Xamarin.Forms.Color.Green;
            finished.Text = "Finished";
        }

        public void sortPokemon(PokemonData pokemon){
            
            if (pokemon.Types[s].Type.Name == "normal")
            {
                NormalArray[nml] = pokemon;
                nml++;
            }
            if (pokemon.Types[s].Type.Name == "fairy")
            {
                FairyArray[fairy] = pokemon;
                fairy++;
            }
            if (pokemon.Types[s].Type.Name == "bug")
            {
                BugArray[bug] = pokemon;
                bug++;
            }
           if (pokemon.Types[s].Type.Name == "dragon")
            {
                DragonArray[dra] = pokemon;
                dra++;
            }
            if (pokemon.Types[s].Type.Name == "ice")
            {
                IceArray[ice] = pokemon;
                ice++;
            }
            if (pokemon.Types[s].Type.Name == "fighting")
            {
                FightingArray[fght] = pokemon;
                fght++;
            }
            if (pokemon.Types[s].Type.Name == "fire")
            {
                FireArray[fire] = pokemon;
                fire++;
            }
            if (pokemon.Types[s].Type.Name == "flying")
            {
                FlyingArray[flying] = pokemon;
                flying++;
            }
            if (pokemon.Types[s].Type.Name == "grass")
            {
                GrassArray[grass] = pokemon;
                grass++;
            }
            if (pokemon.Types[s].Type.Name == "ghost")
            {
                GhostArray[ghost] = pokemon;
                ghost++;
            }
            if (pokemon.Types[s].Type.Name == "ground")
            {
                GroundArray[ground] = pokemon;
                ground++;
            }
            if (pokemon.Types[s].Type.Name == "electric")
            {
                ElectricArray[electric] = pokemon;
                electric++;
            }
            if (pokemon.Types[s].Type.Name == "poison")
            {
                PoisonArray[poison] = pokemon;
                poison++;
            }
            if (pokemon.Types[s].Type.Name == "psychic")
            {
                PsychicArray[psychic] = pokemon;
                psychic++;
            }
            if (pokemon.Types[s].Type.Name == "rock")
            {
                RockArray[rock] = pokemon;
                rock++;
            }
            if (pokemon.Types[s].Type.Name == "water")
            {
                WaterArray[water] = pokemon;
                water++;
            }


        }
        void NormalClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(NormalArray, "Normal"));
        }
        void BugClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(BugArray, "Bug"));
        }
        void DragonClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(DragonArray, "Dragon"));
        }
        void IceClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(IceArray, "Ice"));
        }
        void FightClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(FightingArray, "Fighting"));
        }
        void FireClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(FireArray, "Fire"));
        }
        void FlyClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(FlyingArray, "Flying"));
        }
        void GrassClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(GrassArray, "Grass"));
        }
        void GhostClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(GhostArray, "Ghost"));
        }
        void GroundClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(GroundArray, "Ground"));
        }
        void ElectricClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(ElectricArray, "Electric"));
        }
        void WaterClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(WaterArray, "Water"));
        }
        void RockClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(RockArray, "Rock"));
        }
        void PoisonClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(PoisonArray, "Poison"));
        }
        void PsychicClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(PsychicArray, "Psychic"));
        }
        void FairyClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListOfPokemon(FairyArray, "Fairy"));
        }


    }
}
