﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using PokemonApp;
//
//    var pokemonData = PokemonData.FromJson(jsonString);

namespace PokemonApp
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class PokemonData
    {
        [JsonProperty("forms")]
        public Form[] Forms { get; set; }

        [JsonProperty("abilities")]
        public Ability[] Abilities { get; set; }

        [JsonProperty("stats")]
        public Stat[] Stats { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("weight")]
        public long Weight { get; set; }

        [JsonProperty("moves")]
        public Move[] Moves { get; set; }

        [JsonProperty("sprites")]
        public Sprites Sprites { get; set; }

        [JsonProperty("held_items")]
        public object[] HeldItems { get; set; }

        [JsonProperty("location_area_encounters")]
        public string LocationAreaEncounters { get; set; }

        [JsonProperty("height")]
        public long Height { get; set; }

        [JsonProperty("is_default")]
        public bool IsDefault { get; set; }

        [JsonProperty("species")]
        public Form Species { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("order")]
        public long Order { get; set; }

        [JsonProperty("game_indices")]
        public GameIndex[] GameIndices { get; set; }

        [JsonProperty("base_experience")]
        public long BaseExperience { get; set; }

        [JsonProperty("types")]
        public TypeElement[] Types { get; set; }
    }

    public partial class Ability
    {
        [JsonProperty("slot")]
        public long Slot { get; set; }

        [JsonProperty("is_hidden")]
        public bool IsHidden { get; set; }

        [JsonProperty("ability")]
        public Form AbilityAbility { get; set; }
    }

    public partial class Form
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class GameIndex
    {
        [JsonProperty("version")]
        public Form Version { get; set; }

        [JsonProperty("game_index")]
        public long GameIndexGameIndex { get; set; }
    }

    public partial class Move
    {
        [JsonProperty("version_group_details")]
        public VersionGroupDetail[] VersionGroupDetails { get; set; }

        [JsonProperty("move")]
        public Form MoveMove { get; set; }
    }

    public partial class VersionGroupDetail
    {
        [JsonProperty("move_learn_method")]
        public Form MoveLearnMethod { get; set; }

        [JsonProperty("level_learned_at")]
        public long LevelLearnedAt { get; set; }

        [JsonProperty("version_group")]
        public Form VersionGroup { get; set; }
    }

    public partial class Sprites
    {
        [JsonProperty("back_female")]
        public object BackFemale { get; set; }

        [JsonProperty("back_shiny_female")]
        public object BackShinyFemale { get; set; }

        [JsonProperty("back_default")]
        public string BackDefault { get; set; }

        [JsonProperty("front_female")]
        public object FrontFemale { get; set; }

        [JsonProperty("front_shiny_female")]
        public object FrontShinyFemale { get; set; }

        [JsonProperty("back_shiny")]
        public string BackShiny { get; set; }

        [JsonProperty("front_default")]
        public string FrontDefault { get; set; }

        [JsonProperty("front_shiny")]
        public string FrontShiny { get; set; }
    }

    public partial class Stat
    {
        [JsonProperty("stat")]
        public Form StatStat { get; set; }

        [JsonProperty("effort")]
        public long Effort { get; set; }

        [JsonProperty("base_stat")]
        public long BaseStat { get; set; }
    }

    public partial class TypeElement
    {
        [JsonProperty("slot")]
        public long Slot { get; set; }

        [JsonProperty("type")]
        public Form Type { get; set; }
    }

    public partial class PokemonData
    {
        public static PokemonData FromJson(string json) => JsonConvert.DeserializeObject<PokemonData>(json, PokemonApp.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this PokemonData self) => JsonConvert.SerializeObject(self, PokemonApp.Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
