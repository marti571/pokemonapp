﻿using System;
using System.Collections.Generic;
using PokemonApp.Models;
using Xamarin.Forms;

namespace PokemonApp
{
    public partial class PokemonInfo : ContentPage
    {
        public PokemonInfo(TextCellItem pokemon)
        {
            InitializeComponent();
            BindingContext = pokemon;
        }
    }
}
