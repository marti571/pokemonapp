﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace PokemonApp
{
    public partial class SignUpPage : ContentPage
    {
        public SignUpPage()
        {
            InitializeComponent();
        }
        async void SignUP_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new PokedexPage());
        }
    }
}
