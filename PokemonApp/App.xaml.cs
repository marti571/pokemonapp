using Xamarin.Forms;
using Plugin.Connectivity;
namespace PokemonApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new PokemonAppPage());
        }

        protected override void OnStart()
        {
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingCenter.Send<PokemonAppPage>(null, "No Internet Connection");
                }
            };
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
