﻿using System;

using Xamarin.Forms;

namespace PokemonApp.Models
{
    public class TextCellItem
    {
       public string PokeText
        {
            get;set;
        }
        public ImageSource IconImage
        {
            get;set;
        }
        public int Number
        {
            get;set;
        }
        public long ID
        {
            get;set;
        }
        //more than one move lets just do the first two
        public string Move
        {
            get;set;
        }
        public long Weight
        {
            get;set;
        }
        public long Height
        {
            get;set;
        }
        public string Move1
        {
            get; set;
        }
        public string Move2
        {
            get; set;
        }
    }
}

