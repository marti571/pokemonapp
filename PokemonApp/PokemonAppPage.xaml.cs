﻿using Xamarin.Forms;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PokemonApp
{
    public partial class PokemonAppPage : ContentPage
    {
        public PokemonAppPage()
        {
            InitializeComponent();
        }
        void Login_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());       
        }
        void SignUp_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SignUpPage());
        }
    }
}
