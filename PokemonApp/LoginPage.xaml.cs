using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PokemonApp
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();


        }

        async void Login_Clicked(object sender, EventArgs e)
        {
            if (username.Text == "TeamPokemon" && password.Text == "OurPassword")
            {
                await Navigation.PushAsync(new PokedexPage());
            }
            else
            {
                bool answer = await DisplayAlert("Alert", "Username/password incorrect", "Sign up!", "Try Again");
                if(answer==true)
                {
                    
                    await Navigation.PushAsync(new SignUpPage());
                }
                else
                {}
            }
        }
    }
}
