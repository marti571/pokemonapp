using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Plugin.Connectivity;
using System.Net.Http;
using System.Threading.Tasks;
using PokemonApp.Models;
using Xamarin.Forms;

namespace PokemonApp
{
    public partial class ListOfPokemon : ContentPage
    {
        int count = 1;
        int nonNullCount = 0;
        ObservableCollection<TextCellItem> PList = new ObservableCollection<TextCellItem>();
        public ListOfPokemon(PokemonData[] TypeArray, string TitleText)
        {
            InitializeComponent();
            ListLabel.Text = TitleText;


            PopulateList(TypeArray);

        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listview = (ListView)sender;
            TextCellItem itemTapped = (TextCellItem)listview.SelectedItem;
            Navigation.PushAsync(new PokemonInfo(itemTapped));
        }
        public int ParseArray(PokemonData[] array)
        {

            for (int i = 0; i <= array.Length - 1; i++)
            {
                if (array[i] != null)
                {
                    nonNullCount++;
                }
            }

            return nonNullCount;
        }

        private void PopulateList(PokemonData[] name)
        {
            int howFar = ParseArray(name);
            for (int i = 0; i < howFar;i++)
            {
                var data = new TextCellItem()
                {
                    PokeText = name[i].Name,
                    IconImage = name[i].Sprites.FrontShiny,
                    Number = count,
                    ID = name[i].Id,
                    Weight = name[i].Weight,
                    Height = name[i].Height,
                    Move = name[i].Moves[0].MoveMove.Name,
                    Move1 = name[i].Moves[1].MoveMove.Name,
                    Move2 = name[i].Moves[2].MoveMove.Name,
                };
                PList.Add(data);
                count++;
            }
            PokemonList.ItemsSource = PList;
        }


    }
}